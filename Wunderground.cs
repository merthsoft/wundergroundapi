﻿using Merthsoft.TinyUrlApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Merthsoft.WundergroundApi
{
	public static class Wunderground {
		private const string KEY = "<your key>";
		private const string REF_ID = "<your refid>";
		private const string CONDITIONS_URL = "http://api.wunderground.com/api/" + KEY + "/conditions/q/{0}.xml";
		private const string FULL_CONDITIONS_URL = "http://www.wunderground.com/cgi-bin/findweather/hdfForecast?query={0}&apiref=" + REF_ID;

		public static string GetConditions(string location) {
			location = Uri.EscapeDataString(location);
			string fullUrl = string.Format(FULL_CONDITIONS_URL, location);

			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(string.Format(CONDITIONS_URL, location));
			Response result = null;
			XmlSerializer serializer = new XmlSerializer(typeof(Response));
			using (Stream s = req.GetResponse().GetResponseStream()) {
				try {
					result = (Response)serializer.Deserialize(s);
				} catch {
					return getErrorString(fullUrl);
				}
			}

			if (result.Features.Count == 0 || result.Conditions == null) {
				return getErrorString(fullUrl);
			}

			string weather = string.Format("{0}, {1}, Wind: {2}.", result.Conditions.Weather, result.Conditions.Tempterature, result.Conditions.Wind);
			string forecastUrl = result.Conditions.ForecastUrl;
			if (forecastUrl.Contains("?") && !forecastUrl.EndsWith("/")) {
				forecastUrl += "&apiref=" + REF_ID;
			} else {
				forecastUrl += "?apiref=" + REF_ID;
			}
			return string.Format("{0} ({3}) - {1} More: {2}", result.Conditions.Location.FullName, weather, tinyUrl(forecastUrl), result.Conditions.LocalTimeRfc);
		}

		private static string tinyUrl(string url) {
			string tinyUrl = TinyUrl.GetIsGdUrl(url);
			return string.IsNullOrWhiteSpace(tinyUrl) ? url : tinyUrl;
		}

		private static string getErrorString(string fullUrl) {
			return string.Format("Could not get weather information. Try Wunderground directly: {0}", tinyUrl(fullUrl));
		}
	}
}
