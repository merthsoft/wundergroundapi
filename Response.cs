﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Merthsoft.WundergroundApi {
	[XmlRoot("response")]
	public class Response {
		[XmlElement("version")]
		public string Version { get; set; }

		[XmlElement("termsofService")]
		public string TermsOfService { get; set; }

		[XmlArray("features")]
		[XmlArrayItem("feature")]
		public List<string> Features { get; set; }

		[XmlElement("current_observation")]
		public Conditions Conditions { get; set; }

		public Response() {
			Features = new List<string>();
		}
	}
}
