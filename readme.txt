﻿Merthsoft.WundergroundApi
Merthsoft Creations, 2014
Shaun McFall

A simple wrapper around the Wunderground API.

To use to include a reference to Merthsoft.WundergroundApi and call
Wunderground.GetConditions("your location here"). Requires a reference
to Merthsoft.TinyUrlApi (https://bitbucket.org/merthsoft/tinyurlapi) to
generate a small URL when returning results.

Make sure you change the app id and ref id in the code to be your app id
and ref id. Ideally the function call would take that so it's not compiled
in, but that wasn't on my mind when I was making this.

This just returns a fairly limited response for the conditions of the
location you pass into it. For more complicated stuff you can enumerate
over the conditions. Additional development would need to be added to do
more complicated stuff (forecast, etc.).