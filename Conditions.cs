﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Merthsoft.WundergroundApi {
	public class Conditions {
		[XmlElement("image")]
		public Logo Logo { get; set; }

		[XmlElement("display_location")]
		public Location Location { get; set; }

		//[XmlElement("observation_location")]
		//public Location ObservationLocation { get; set; }

		[XmlElement("estimated")]
		public string Estimated { get; set; }

		[XmlElement("station_id")]
		public string StationId { get; set; }

		[XmlElement("observation_time")]
		public string ObservationTime { get; set; }

		[XmlElement("observation_time_rfc822")]
		public string ObservationRfd { get; set; }

		[XmlIgnore]
		public DateTime ObservationDateTime { get { return DateTime.Parse(ObservationRfd); } }

		[XmlElement("observation_epoch")]
		public uint ObservationEpoch { get; set; }

		[XmlElement("local_time_rfc822")]
		public string LocalTimeRfc { get; set; }

		[XmlIgnore]
		public DateTime LocalTime { get { return DateTime.Parse(LocalTimeRfc); } }

		[XmlElement("local_epoch")]
		public uint LocalEpoch { get; set; }

		[XmlElement("local_tz_short")]
		public string LocalTimezoneShort { get; set; }

		[XmlElement("local_tz_long")]
		public string LocalTimezoneLong { get; set; }

		[XmlElement("local_tz_offset")]
		public string LocalTimezoneOffset { get; set; }

		[XmlElement("weather")]
		public string Weather { get; set; }

		[XmlElement("temperature_string")]
		public string Tempterature { get; set; }

		[XmlElement("temp_f")]
		public string TemperatureFarenheit { get; set; }

		[XmlElement("temp_c")]
		public string TemperatureCelcius { get; set; }

		[XmlElement("relative_humidity")]
		public string RelativeHumidity { get; set; }

		[XmlElement("wind_string")]
		public string Wind { get; set; }

		[XmlElement("wind_dir")]
		public string WindDirection { get; set; }

		[XmlElement("wind_degrees")]
		public string WindTemperature { get; set; }

		[XmlElement("wind_mph")]
		public string WindMph { get; set; }

		[XmlElement("wind_gust_mph")]
		public string WindGustMph { get; set; }

		[XmlElement("wind_kph")]
		public string WindKph { get; set; }

		[XmlElement("wind_gust_kph")]
		public string WindGustKph { get; set; }

		[XmlElement("pressure_mb")]
		public string PressureMb { get; set; }

		[XmlElement("pressire_in")]
		public string PressureIn { get; set; }

		[XmlElement("pressure_trend")]
		public string PressureTrend { get; set; }

		[XmlElement("dewpoint_string")]
		public string Dewpoint { get; set; }

		[XmlElement("dewpoint_f")]
		public string DewpointTemperatureFarenheit { get; set; }

		[XmlElement("dewpoint_c")]
		public string DewpointTemperatureCelcius { get; set; }

		[XmlElement("heat_index_string")]
		public string HeatIndex { get; set; }

		[XmlElement("heat_index_f", IsNullable=true)]
		public string HeatIndexTemperatureFarenheit { get; set; }

		[XmlElement("heat_index_c", IsNullable = true)]
		public string HeatIndexTemperatureCelcius { get; set; }

		[XmlElement("windchill_string")]
		public string WindChill { get; set; }

		[XmlElement("windchill_f")]
		public string WindChillTemperatureFarenheit { get; set; }

		[XmlElement("windchill_c", IsNullable = true)]
		public string WindChillTemperatureCelcius { get; set; }

		[XmlElement("feelslike_string")]
		public string FeelsLike { get; set; }

		[XmlElement("feelslike_f", IsNullable = true)]
		public string FeelsLikeTemperatureFarenheit { get; set; }

		[XmlElement("feelslike_c", IsNullable = true)]
		public string FeelsLikeTemperatureCelcius { get; set; }

		[XmlElement("visibility_mi")]
		public string VisibilityMi { get; set; }

		[XmlElement("visibility_km")]
		public string VisibilityKm { get; set; }

		[XmlElement("solarradiation")]
		public string SolarRadiation { get; set; }

		[XmlElement("UV")]
		public string Uv { get; set; }

		[XmlElement("precip_1hr_string")]
		public string Precipitation1Hr { get; set; }

		[XmlElement("precip_1hr_in")]
		public string Precipitation1HrIn { get; set; }

		[XmlElement("precip_1hr_metric")]
		public string Precipitation1HrMetric { get; set; }

		[XmlElement("precip_today_string")]
		public string PrecipitationToday { get; set; }

		[XmlElement("precip_today_in")]
		public string PrecipitationTodayIn { get; set; }

		[XmlElement("precip_today_metric")]
		public string PrecipitationTodayMetric { get; set; }

		[XmlElement("icon")]
		public string Icon { get; set; }

		[XmlElement("icon_url")]
		public string IconUrl { get; set; }

		[XmlElement("forecast_url")]
		public string ForecastUrl { get; set; }

		[XmlElement("history_url")]
		public string HistoryUrl { get; set; }

		[XmlElement("ob_url")]
		public string ObUrl { get; set; }
	}
}
