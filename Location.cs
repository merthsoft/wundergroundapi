﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Merthsoft.WundergroundApi {
	public class Location {
		[XmlElement("full")]
		public string FullName { get; set; }

		[XmlElement("city")]
		public string City { get; set; }

		[XmlElement("state")]
		public string State { get; set; }

		[XmlElement("state_name", IsNullable = true)]
		public string StateFullName { get; set; }

		[XmlElement("country")]
		public string CountryName { get; set; }

		[XmlElement("country_iso3166")]
		public string CountryCode { get; set; }

		[XmlElement("zip", IsNullable = true)]
		public int? ZipCode { get; set; }

		[XmlElement("magic", IsNullable = true)]
		public string Magic { get; set; }

		[XmlElement("wmo", IsNullable = true)]
		public string Wmo { get; set; }

		[XmlElement("latitude")]
		public decimal Latitude { get; set; }

		[XmlElement("longitude")]
		public decimal Longitude { get; set; }

		[XmlElement("elevation")]
		public decimal Elevation { get; set; }

	}
}
